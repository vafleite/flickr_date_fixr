import flickrapi
import webbrowser

api_key = u'<api_key>'
api_secret = u'<api_secret>'
permission = u'write'

flickr = flickrapi.FlickrAPI(api_key, api_secret, format='parsed-json')

print('Step 1: Authenticate')

# Only do this if we don't have a valid token already
if not flickr.token_valid(perms=permission):

    # Get a request token
    flickr.get_request_token(oauth_callback='oob')

    # Open a browser at the authentication URL. Do this however
    # you want, as long as the user visits that URL.
    authorize_url = flickr.auth_url(perms=permission)
    webbrowser.open_new_tab(authorize_url)

    # Get the verifier code from the user. Do this however you
    # want, as long as the user gives the application the code.
    verifier = unicode(raw_input('Verifier code: '))

    # Trade the request token for an access token
    flickr.get_access_token(verifier)


user = u'victor.furtadoleite'
set_id = u'72157657399702061'

res = flickr.people.findByUsername(username=user)
uid = res['user']['id']

res = flickr.photosets.getPhotos(photoset_id=set_id, user_id=uid)

error_list = []

for photo in res['photoset']['photo']:
    print 'Photo %s:' % photo['title']
    exif = flickr.photos.getExif(photo_id=photo['id'])

    if exif['stat'] == u'ok':
        print 'EXIF: {:>7}'.format('OK')
        date = exif['photo']['exif'][1]['raw']['_content']
        date = date.replace(':', '-', 2)
        date_res = flickr.photos.setDates(
            photo_id=photo['id'], date_taken=date
        )

        if date_res['stat'] == u'ok':
            print 'Date: {:>7}'.format('OK')
            rem = flickr.photosets.removePhoto(
                photoset_id=set_id, photo_id=photo['id']
            )

            if rem['stat'] != u'ok':
                print "Error removing photo {} from album. ".format(
                    photo['title']
                )

                if photo['id'] not in error_list:
                    error_list.append(photo['id'])

            else:
                print 'Album: {:>7}'.format('OK')

        else:
            print "Error setting date on photo %s. " % photo['title']

            if photo['id'] not in error_list:
                error_list.append(photo['id'])

    else:
        if photo['id'] not in error_list:
            error_list.append(photo['id'])

    print '\n'

if error_list:
    print 'There are errors.'
    print error_list

